class PackageRepository
  include Elasticsearch::Persistence::Repository
  include Elasticsearch::Persistence::Repository::DSL
  
  index_name 'packages'
  klass Package
  
  analyzed_and_raw = { fields: {
    raw: { type: 'keyword' }
    } 
   }
  
  mapping do 
    indexes :category
    indexes :name
    indexes :name_sort
    indexes :atom
    indexes :description
    indexes :longdescripton
    indexes :homepage
    indexes :license
    indexes :licenses
    indexes :herds
    indexes :maintainers, { type: 'object' } # array
    indexes :useflags, { type: 'object' } # hash
    indexes :metadata_hash
  end
  
  def deserialize(document)
    package = super
    package.id = document['_id']
    package
  end
  
  def serialize(package)
    package.validate!
    package.to_hash.tap do |hash|
      suggest = { name: { input: [ hash[:name] ] } }
      suggest[:category] = { input: hash[:category].collect(&:strip) } if hash[:category].present?
      hash.merge!(:package_suggest => suggest)
      end
      hash.merge!(:package_suggest)
        end
    end
  end
end
