class UseflagSerializer < JSONAPI::Serializable::Resource
  type 'useflags'

  attributes :id
end
