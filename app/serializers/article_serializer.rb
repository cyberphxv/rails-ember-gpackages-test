class ArticleSerializer < JSONAPI::Serializable::Resource
  type 'articles'

  attributes :id, :author, :price, :isbn, :title
end
