class ChangeSerializer < JSONAPI::Serializable::Resource
  type 'changes'

  attributes :id, :package, :category, :change_type, :version, :arches, :commit
end
