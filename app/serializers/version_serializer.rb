class VersionSerializer < JSONAPI::Serializable::Resource
  type 'versions'

  attributes :id
end
