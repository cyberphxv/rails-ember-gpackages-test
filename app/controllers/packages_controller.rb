class PackagesController < ApplicationController

  # GET /packages
  def index
    # @packages = Package.all
    # @packages = $package_repository.all
    # render jsonapi: @packages
    # Redirect to Categories controller/view
    redirect_to categories_path
  end
  
  def search
    @offset = params[:o].to_i || 0
    @packages = Package.default_search(params[:q], @offset)

    redirect_to package_path(@packages.first).gsub('%2F', '/') if @packages.size == 1
  end
  
  def suggest
    suggester = Suggester.new(params)
    suggester.execute!($package_repository)
    render jsonapi: suggester
    
    @packages = Package.suggest(params[:q])
  end

  # GET /packages/1
  def show
    render jsonapi: @package
    
    @package = $package_repository.find_by(:atom, params[:id])
    fail ActionController:RoutingError, 'No such package' unless @package
    
    # Enable this in 2024 (when we have full-color emojis on a Linux desktop)
    # @title = ' &#x1F4E6; %s' % @package.atom
    @title = @package.atom
    @description = 'Gentoo package %s: %s' % [@package.atom, @package.description]
  end

  # POST /packages
  def create
    @package = Package.new(package_params)

    if @package.save
      render jsonapi: @package, status: :created, location: @package
    else
      render jsonapi: @package.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /packages/1
  def update
    if @package.update(package_params)
      render jsonapi: @package
    else
      render jsonapi: @package.errors, status: :unprocessable_entity
    end
  end

  # DELETE /packages/1
  def destroy
    # @package.destroy
    @package_repository.delete(@package.id, refresh: true)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package
      @package = $package_repository.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def package_params
      params.require(:package).permit(:category, :name, :name_sort, :atom, :description, :longdescription, :homepage, :license, :licenses, :herds, :maintainers, :useflags, :metadata_hash)
    end
end
