class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :search]

  # GET /categories
  def index
    # @categories = Category.all
    # render json: @categories
    # render jsonapi: Category.all
    @categories = $category.repository.all sort: 'name.raw' #.all_sorted_by(:name :asc)
  end

  # GET /categories/1
  def show
    # render jsonapi: @category
    @packages = Rails.cache.fetch("category/#{@category.name}/packages", expires_in: 10.minutes) do
      $package_repository.find_by_all(:category, $category_repository.name, sort: {
        name_sort: {
          order: 'asc' }
        }).map do |pkg|
          pkg.to_os(:name, :atom, :descripton)
        end
    end
    @description = t(:desc_categories_show,
      category: $category_repository.name,
      description: $category_repository.description)
  end
  
  def search
  end

  # POST /categories
  def create
    @category = Category.new(category_params)

    if @category.save
      render jsonapi: @category, status: :created, location: @category
    else
      render jsonapi: @category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /categories/1
  def update
    if @category.update(category_params)
      render jsonapi: @category
    else
      render jsonapi: @category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /categories/1
  def destroy
    @category_repository.delete(@category.id, refresh: true)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = $category_repository.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def category_params
    params.require(:category).permit(:name, :description, :metadata_hash)
  end
end
