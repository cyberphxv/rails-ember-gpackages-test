//= require jquery
//= require jquery_ujs
//= require ./environment
//= require ember
//= require ember-data
//= require active-model-adapter

//= require_self
//= require ./blog-api

// for more details see: http://emberjs.com/guides/application/
BlogApi = Ember.Application.create({rootElement: '#ember-app'});
