// Override the default adapter with the `DS.ActiveModelAdapter` which
// is built to work nicely with the ActiveModel::Serializers gem.
//
import DS from 'ember-data';

BlogApi.ApplicationAdapter = DS.ActiveModelAdapter.extend({
	host: 'http://localhost:3000',
  dataType: "json"
});
