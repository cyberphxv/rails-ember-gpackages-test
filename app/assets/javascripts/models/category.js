// for more details see: http://emberjs.com/guides/models/defining-models/

BlogApi.Category = DS.Model.extend({
  name: DS.attr('string'),
  description: DS.attr('string'),
  metadataHash: DS.attr('string')
});
