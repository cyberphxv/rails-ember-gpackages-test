// for more details see: http://emberjs.com/guides/models/defining-models/

BlogApi.Article = DS.Model.extend({
  author: DS.attr('string'),
  price: DS.attr('number'),
  isbn: DS.attr('number'),
  title: DS.attr('string')
});
