# require 'elasticsearch/model'

class Package < ApplicationRecord
  # include Elasticsearch::Model
  include ActiveModel::Model # supplies some methods necessary for form helpers
  include ActiveModel::Validations # provides error message caching and validations methods
  
  serialize :maintainers, Hash

  # belongs_to :category

  # index_name    "packages"
  # document_type "package"
  
  # /app/repository/package.rb
  validate :category, true
  
  ATTRIBUTES = [:category,
    :name,
    :name_sort,
    :atom,
    :description,
    :longdescription,
    :homepage,
    :license,
    :licenses,
    :herds,
    :maintainers,
    :useflags,
    :metadata_hash]
  attr_accessor(*ATTRIBUTES)
  # attr_reader :attributes
  attr_reader :category, :name, :path
  
  def initialize(attr={})
    attr.each do |k,v|
      if ATTRIBUTES.include?(k.to_sym)
        send("#{k}=", v)
      end
    end
  end
  
  def attributes
    ATTRIBUTES.inject({}) do |hash, attr|
      if value = send(attr)
        hash[attr] = value
      end
      hash
    end
  end
  alias :to_hash :attributes
end
end
