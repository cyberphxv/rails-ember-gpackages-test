require 'elasticsearch/model'

class Category < ApplicationRecord
  include Elasticsearch::Model

  index_name    "categories"
  document_type "category"

end
